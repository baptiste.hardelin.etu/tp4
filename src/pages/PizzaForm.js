import Router from '../Router.js';
import Page from './Page.js';

export default class PizzaForm extends Page {
	render() {
		return /*html*/ `
		<form class="pizzaForm">
		<label>
			Nom :
			<input type="text" name="name">
		</label>
		<label>
			Image :<br/>
			<input type="text" name="image" placeholder="https://source.unsplash.com/xxxxxxx/600x600">
			<small>Vous pouvez trouver des images de pizza sur <a href="https://unsplash.com/">https://unsplash.com/</a> puis utiliser l'URL <code>https://source.unsplash.com/xxxxxxx/600x600</code> où <code>xxxxxxx</code> est l'id de la photo choisie (celui dans la barre d'adresse)</small>
		</label>
		<label>
			Prix petit format :
			<input type="number" name="price_small" step="0.05">
		</label>
		<label>
			Prix grand format :
			<input type="number" name="price_large" step="0.05">
		</label>
		<button type="submit">Ajouter</button>
	</form>`;
	}

	mount(element) {
		super.mount(element);
		// une fois la page affichée, on détecte la soumission du formulaire
		const form = this.element.querySelector('form');
		form.addEventListener('submit', event => this.submit(event));
	}

	submit(event) {
		event.preventDefault(); // bloque le rechargement de la page
		const nameInput = this.element.querySelector('input[name=name]'); // champ de saisie "nom"
		const imageInput = this.element.querySelector('input[name=image]');
		const price_smallInput = this.element.querySelector('input[name=price_small]');
		const price_largeInput = this.element.querySelector('input[name=price_large]');

		const pizza = {
			name: nameInput.value,
			image: imageInput.value,
			price_small: price_smallInput.value,
			price_large: price_largeInput.value
		}

		// si l'utilisateur n'a rien tapé dans le champ
		// on affiche un message d'erreur
		if (nameInput.value === '') {
			alert('Erreur : le champ "Nom" est obligatoire');
			return;
		}
		// sinon on affiche un message de succès
		alert(`La pizza ${nameInput.value} a été ajoutée avec succès`);
		// on vide le champ
		nameInput.value = '';
		fetch(
			'http://localhost:8080/api/v1/pizzas',
			{
				method:'POST',
				headers: { 'Content-Type': 'application/json' },
				body: JSON.stringify(pizza)
			}
		).then(
			(response) => Router.navigate('/')
		);

	}
}
